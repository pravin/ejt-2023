
import java.io.*;
import java.net.*;

public class DummyServer {
   public static void main(String[] args) throws Exception{
      ServerSocket ss = new ServerSocket(Integer.parseInt(args[0]));
      while (true) {
         Socket clientSocket = ss.accept();
         Socket serverSocket = new Socket(args[1],Integer.parseInt(args[2]));
         System.out.println("connected to server");
         Thread th1 = new Thread(new InOut1(clientSocket.getInputStream(),
                                          serverSocket.getOutputStream(),"Client"));
         Thread th2 = new Thread(new InOut1(serverSocket.getInputStream(),
                                          clientSocket.getOutputStream(),"Server"));
         th1.start();
         th2.start();
      }
   }
}
