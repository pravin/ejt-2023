
package org.rollwala.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class HelloServletNew extends HttpServlet {
    public void init() {
        // some initialization
        // acquire some resources
    }
    public void destroy() {
        // release any resources, which were acquired
    }
    public String getServletInfo() {
        return "HelloServlet demo Copyright RCC, GU - 2023";
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ServletContext context = getServletContext();
        String greeting = getServletConfig().getInitParameter("greeting");
        String driverClassName = 
            context.getInitParameter("jdbc.driver.classname");
        String displayName = context.getServletContextName();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Hello from RCC </title></head>");
        out.println("<body><h1>"+greeting+" world</h1>");
        out.println(" <h2>Display name "+displayName+"</h2>");
        out.println(" <h3>Driver class name "+driverClassName+"</h3></body>");
    }
}

