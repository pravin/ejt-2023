
package org.rollwala.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class DisplayServlet extends HttpServlet {
    public void init() {
        // some initialization
        // acquire some resources
    }
    public void destroy() {
        // release any resources, which were acquired
    }
    public String getServletInfo() {
        return "HelloServlet demo Copyright RCC, GU - 2023";
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ServletContext application = getServletContext();
        ServletConfig config = getServletConfig();
        String displayName = application.getServletContextName();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Display Servlet</title></head>");
        out.println("<body><h1>Display Page</h1>");
        out.println("<h2>Attributes in Request from Previous servlet</h2>");
        out.println("<table>");
        out.println("<tr><td>name</td><td>value</td></tr>");
        Enumeration<String> attributeNames = request.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String name = attributeNames.nextElement();
            Object value = request.getAttribute(name);
            out.println("<tr><td>"+name+"</td><td>"+value+"</td></tr>");
        }
        out.println("</table>");
        out.println("</body>");
        out.println("</html>");
    }
}

