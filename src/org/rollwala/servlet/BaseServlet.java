
package org.rollwala.servlet;

import javax.servlet.*;
import java.io.*;
import java.util.Enumeration;

public abstract class BaseServlet implements Servlet, ServletConfig {
    private ServletConfig config;
    public final void init(ServletConfig config) {
        this.config = config;
        init();
    }
    public void init() { }
    public final ServletConfig getServletConfig() {
        return this.config;
    }
    public void destroy() {
    }
    public String getServletInfo() {
        return "";
    }
    public final String getInitParameter(String pname) {
        return this.config.getInitParameter(pname);
    }
    public final Enumeration<String> getInitParameterNames() {
        return this.config.getInitParameterNames();
    }
    public final ServletContext getServletContext() {
        return this.config.getServletContext();
    }
    public final String getServletName() {
        return this.config.getServletName();
    }
    public final void log(String msg) {
        getServletContext().log(msg);
    }
    public final void log(String msg, Throwable exception) {
        getServletContext().log(msg, exception);
    }
}

