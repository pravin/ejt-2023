
package org.rollwala.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class ProcessingServlet extends HttpServlet {
    public void init() {
        // some initialization
        // acquire some resources
    }
    public void destroy() {
        // release any resources, which were acquired
    }
    public String getServletInfo() {
        return "ProccessingServlet demo Copyright RCC, GU - 2023";
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ServletContext context = getServletContext();
        Map<String,String> servletInitParams = new HashMap<>();
        Enumeration<String> initParamNames = getServletConfig().getInitParameterNames();
        while (initParamNames.hasMoreElements()) {
            String paramName = initParamNames.nextElement();
            String paramValue = getServletConfig().getInitParameter(paramName);
            servletInitParams.put(paramName, paramValue);
        }
        request.setAttribute("servlet params", servletInitParams);
        
        Map<String,String> contextInitParams = new HashMap<>();
        Enumeration<String> contextParamNames = getServletContext().getInitParameterNames();
        while (contextParamNames.hasMoreElements()) {
            String paramName = contextParamNames.nextElement();
            String paramValue = getServletContext().getInitParameter(paramName);
            contextInitParams.put(paramName, paramValue);
        }
        request.setAttribute("context params", contextInitParams);
//        RequestDispatcher rd = getServletContext().getNamedDispatcher("DisplayServlet");
//        RequestDispatcher rd = getServletContext().getNamedDispatcher("My First Servlet");
        RequestDispatcher rd = getServletContext().getNamedDispatcher("Hello JSP Servlet");
        rd.forward(request, response);
    }
}

