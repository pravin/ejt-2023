
package org.rollwala.servlet;

import javax.servlet.*;
import java.io.*;

public class HelloServlet implements Servlet {
    private ServletConfig config;
    public void init(ServletConfig config) {
        this.config = config;
    }
    public ServletConfig getServletConfig() {
        return this.config;
    }
    public void destroy() {
    }
    public String getServletInfo() {
        return "HelloServlet demo Copyright RCC, GU - 2023";
    }
    public void service(ServletRequest request, ServletResponse response) throws IOException {
        ServletContext context = this.config.getServletContext();
        String greeting = this.config.getInitParameter("greeting");
        String driverClassName = 
            context.getInitParameter("jdbc.driver.classname");
        String displayName = context.getServletContextName();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Hello from RCC </title></head>");
        out.println("<body><h1>"+greeting+" world</h1>");
        out.println(" <h2>Display name "+displayName+"</h2>");
        out.println(" <h3>Driver class name "+driverClassName+"</h3></body>");
    }
}

