
package org.rollwala.academic.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class LoginServlet extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String loginId = request.getParameter("loginid");
        String password = request.getParameter("passwd");
        String remember = request.getParameter("remember");
        
        // if loginid and password combination is not valid
        // set a error message in the request and forward to
        // login.jsp

        UserDao userDao = new UserDao();
        UserInfo user = userDao.authenticate(loginId, password);
        if (user == null) {
            request.setAttribute("message", "incorrect username/password");
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
            dispatcher.forward(request, response);
            return;
        }
        
        if (remember != null) { // checkbox is checked or not
            response.addCookie(new Cookie("loginid", loginId));
            response.addCookie(new Cookie("passwd", password));
        }
        // discard any earlier session before creating a new session
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        session = request.getSession();
        session.setAttribute("user", user);
        List<String> cartProductNames = new ArrayList<>();
        session.setAttribute("cart", cartProductNames);
        
        RequestDispatcher dispatcher = getServletContext().getNamedDispatcher("homeJSP");
        dispatcher.forward(request, response);
    }
}

