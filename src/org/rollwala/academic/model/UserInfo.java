
package org.rollwala.academic.model;

public class UserInfo {
    private String name;
    private String address;
    private String city;

    public UserInfo(String name, String address, String city) {
        this.name = name;
        this.address = address;
        this.city = city;
    }
    public String getName() {
        return this.name;
    }
    public String getAddress() {
        return this.address;
    }
    public String getCity() {
        return this.city;
    }
}

