
package org.rollwala.academic.dao;

import java.util.*;
import org.rollwala.academic.model.UserInfo;

public class UserDao {
/*
    Initially managing a Map for fetching Userdata.
    Map<String,String> passwords
    Map<String,UserInfo> userInfos
    
*/
    private Map<String,String> passwords = Map.ofEntries(
            Map.entry("james","jag2024"),
            Map.entry("joshua", "josh2024"),
            Map.entry("arvind", "arvind2024")
        );

    private Map<String,UserInfo> userInfos = Map.ofEntries(
            Map.entry("james", new UserInfo("James Gosling", "Amazon",  "Seattle")),
            Map.entry("joshua", new UserInfo("Joshua Bloch", "Address1", "California")),
            Map.entry("arvind", new UserInfo("Arvind Vaish", "California", "California"))
        );

    public UserInfo authenticate(String loginid, String password) {
        String passwd = passwords.get(loginid);
        if (!password.equals(passwd)) {
            return null;
        }
        return userInfos.get(loginid);
    }
}

