
package org.rollwala.academic.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class WelcomeServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext()
//                .getRequestDispatcher("/WEB-INF/jsp/welcome.jsp");
                .getNamedDispatcher("WelcomeJSP");
        dispatcher.forward(request, response);
    }
}



