
package org.rollwala.academic.servlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import org.rollwala.academic.model.UserInfo;

public class LogoutServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            UserInfo user = (UserInfo)session.getAttribute("user");
            if (user != null) {
                String message = "user "+user.getName()+" signed out";
                request.setAttribute("message", message);
            }
            session.invalidate();
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
        dispatcher.forward(request, response);
    }
}

