
import java.io.*;

public class InOut1 implements Runnable {
   BufferedReader br;
   PrintWriter    pw;
   PrintWriter    logWriter;
   String   prompt;
   public InOut1(InputStream in , OutputStream out, String id) throws Exception{
      br = new BufferedReader(new InputStreamReader(in));
      pw = new PrintWriter(out,true);
      logWriter=LogWriter.getLogWriter();
      prompt = id;
   }
   public void run() {
     try {
      String inLine = br.readLine();
      while (inLine != null) {
         pw.println(inLine);
         System.out.println(prompt+">"+inLine);
         logWriter.println(prompt+">"+inLine);
         inLine = br.readLine();
      }
     }
     catch (Exception e) { e.printStackTrace(); }
   }
}
