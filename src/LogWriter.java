

import java.io.*;
import java.net.*;

public class LogWriter implements Runnable {
   PrintWriter       pw;
   BufferedReader    br;
   PrintWriter       logwr;
   private static LogWriter logWriter;
   private LogWriter(String flname) throws Exception{
      logwr = new PrintWriter(new FileOutputStream(flname,true),true);
      PipedInputStream pis = new PipedInputStream();
      PipedOutputStream pos = new PipedOutputStream(pis);
      br = new BufferedReader(new InputStreamReader(pis));
      pw = new PrintWriter(pos,true);
   }
   public static PrintWriter getLogWriter() {
      return logWriter.pw;
   }
   public void run() {
      try {
         String inLine = br.readLine();
         while (inLine != null) {
            logwr.println(inLine);
            inLine=br.readLine();
         }
         br.close();
//         logwr.close();
      }
      catch (Exception e) {
         e.printStackTrace();

      }
   }
   static {
      try {
         logWriter = new LogWriter("LogWriter.log");
         new Thread(logWriter).start();
      }
      catch (Exception e) {
         e.printStackTrace();
      }
   }
}
