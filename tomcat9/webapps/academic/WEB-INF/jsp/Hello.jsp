<%@ page import="java.util.*" %>
<%! private static int SOME_CONSTANT = 25; %>
<%!    public int getConstant() {
        return SOME_CONSTANT;
    }
%>
<html>
    <head><title>Hello Jsp</title></head>
    <body>
        <h1>Hello Jsp</h1>
        <h2>Demo of JSP</h2>
        A JSP file can contains all kinds of html tags.
        <h3>The display name for application is:
        <%-- the following is an example of a jsp expression. --%>
        <%= application.getServletContextName() %></h3>
        <%-- the following is an example of a jsp scriplet --%>
        <%
             System.out.println("not going to browser");
             out.println("This comes on the browser from scriplet ");
             out.println("<b>"+request.getMethod()+"</b>");
             out.println(" constant: "+getConstant());
        %>
    </body>
</html>

