<%@ page import="java.util.*,org.rollwala.academic.model.UserInfo" %>
<%
UserInfo user = (UserInfo)session.getAttribute("user");
%>
<html>
    <head><title>Home Page</title></head>
    <body>
        <h1>Home Page</h1>
        <a href="Logout">Sign out</a>
        <h3> Welcome <%= user.getName() %> from <%= user.getCity() %></h3>
        <p/>
        List of product names selected
        <table>
        <tr><td>Product Name</td></tr>
<%
    List<String> productNames = (List<String>)session.getAttribute("cart");
    for (String productName : productNames) {
%>
        <tr><td><%= productName %> </td></tr>
<%  } %>
        </table>
        
    </body>
</html>

